Satelicom Control System GraphQL Player v. 1.1
==============================================

This is a tool to test the SCS GraphQL endpoint. Once configured, you
can use the browser inspector to see how to submit the request to the SCS endpoint.

Please follow the following instructions in order to configure the test
environment.

# Oauth2 Token

1. On line 3 of the file `ScsUtils.js`, replace the string `PUT-HERE-YOUR-TOKEN` with the provided token
2. (Optional) If necessary, on line 4 replace the production url with a different one
3. Open the file index.html in your browser (Chrome or Firefox)

# JWT Token

1. On line 3 of the file `ScsUtils.js`, replace the string `PUT-HERE-YOUR-TOKEN` with the JWT token
2. On line 5 of the file `ScsUtils.js`, replace the string `Bearer` with `Token`
3. (Optional) If necessary, on line 4 replace the production url with a different one
4. Open the file index.html in your browser (Chrome or Firefox)
