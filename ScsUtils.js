class ScsUtils {
  constructor() {
    this.token = "PUT-HERE-YOUR-TOKEN";
    this.endpoint = "https://www.satelicom.com/api/graphql";
    this.authorization = "Bearer";
  }

  getToken() {
    return this.token;
  }

  getEndpoint() {
    return this.endpoint;
  }

  getAuthorization() {
    return this.authorization;
  }
}
